<?php
/**
 * @file
 *   Views relationship handler for exposing a unified view of node field diffs.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */
namespace Drupal\revision_field_views\Views;

/**
 * Relationship handler to obtain access to field diffs between two revisions.
 *
 * @ingroup views_relationship_handlers
 */
class views_handler_relationship_diff extends \views_handler_relationship {
  /**
   * The number of field name placeholders that have been used so far.
   *
   * This is needed to workaround an issue with UNION queries not playing well
   * with query placeholders.
   *
   * @var int
   */
  private $field_placeholder_counter;

  /**
   * Constructor for views_handler_relationship_diff.
   */
  public function __construct() {
    $this->field_placeholder_counter = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options                = parent::option_definition();
    $options['diff_fields'] = array('default' => array());

    return $options;
  }

  /**
   * {@inheritDoc}
   *
   * Provides the ability to configure which fields are diff-ed.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $this->filter_relationship_options($form);
    $form['diff_fields'] = $this->build_diff_fields_selector();
  }

  /**
   * {@inheritDoc}
   *
   * Ensures that there is a "Preceding revision" relationship in the view.
   */
  public function validate() {
    $errors = parent::validate();

    $relationships = $this->view->display_handler->get_option('relationships');

    $relationship_id    = $this->options['relationship']    ?? '';
    $relationship_info  = $relationships[$relationship_id]  ?? NULL;
    $relationship_table = $relationship_info['table']       ?? NULL;

    if ($relationship_table !== 'node_revision_sequence') {
      $errors[] =
        t('The "@relationship_selector" of the "@current_relationship" relationship must be a "Content revision: Preceding revision" relationship.',
          array(
            '@relationship_selector'  => t('Relationship (older revision)'),
            '@current_relationship'   => $this->label(),
          ));
    }

    return $errors;
  }

  /**
   * {@inheritDoc}
   */
  public function query() {
    $field_diff_query   = $this->build_diff_query();

    // $this->relationship is the table alias for the preceding/"old" revision.
    $old_revision_alias = $this->relationship;
    $new_revision_alias = $this->get_table_alias_of_new_node_revision();

    $join_type = empty($this->options['required']) ? 'LEFT' : 'INNER';

    // Join: from node_revision to the field diff query, based on node ID.
    // The join is aliased as "revision_diff".
    $join_definition_diffs_via_new_rev_nid = array(
      'type'  => $join_type,

      'table'         => 'revision_diff_field',
      'table formula' => $field_diff_query,
      'field'         => 'entity_id',

      'left_table'  => $new_revision_alias,
      'left_field'  => 'nid',
    );

    $join             = new \views_join();
    $join->definition = $join_definition_diffs_via_new_rev_nid;
    $join->adjusted   = TRUE;

    $join->construct();

    $this->alias =
      $this->query->add_relationship(
        'revision_diff',
        $join,
        'revision_diff_field',
        $this->relationship
      );

    $this->query->add_where_expression(
      0,
      "({$this->alias}.old_vid IS NULL OR" .
      " {$this->alias}.old_vid = {$old_revision_alias}.vid".
      ") AND " .
      "{$this->alias}.new_vid = {$new_revision_alias}.vid"
    );
  }

  /**
   * Builds a UNION query that calculates differences for all node fields.
   *
   * The query returns the following fields:
   *   - entity_id:   The unique identifier for the node.
   *   - old_vid:     The previous revision ID.
   *   - new_vid:     The new revision ID.
   *   - field_name:  The machine name of the field.
   *   - field_title: The human-friendly title of the field.
   *   - old_value:   The value of the field in the previous revision.
   *   - new_value:   The value of the field in the new revision.
   *
   * @return \SelectQueryInterface
   *   The differences query.
   */
  protected function build_diff_query() {
    $enabled_diff_fields = $this->options['diff_fields'] ?? array();
    $fields_query_info   = $this->get_all_node_fields_query_info();

    /** @var \SelectQueryInterface $field_query */
    $field_query = NULL;

    foreach ($fields_query_info as $field_id => $query_info) {
      if ((count(array_filter($enabled_diff_fields)) > 0)
          && empty($enabled_diff_fields[$field_id])) {
        continue;
      }

      $query = $this->build_field_query($query_info);

      if ($field_query === NULL) {
        $field_query = $query;
      }
      else {
        $field_query = $field_query->union($query, 'ALL');
      }
    }

    $union_query =
      \Database::getConnection()
        ->select($field_query)
        ->fields(
          NULL,
          array(
            'entity_id',
            'old_vid',
            'new_vid',
            'field_name',
            'field_title',
            'old_value',
            'new_value'
          )
        );

    return $union_query;
  }

  /**
   * Gets the table alias assigned by Views for the "new" node revision.
   *
   * $this->relationship is a `node_revision` JOIN-ed into the query by the
   * "Preceding revision" relationship. As such, the relationship is to the
   * "old" node revision. We want to navigate from that relationship to the
   * node revision that represents the "new" `node_revision`. So, we need to
   * get the relationship that was originally provided to the
   * "Preceding revision" relationship.
   *
   * The JOIN structure looks like this:
   * - `node_revision` ("new" revision)
   *   - `node_revision_sequence` (joined to "new" revision based on `new_vid`
   *     column)
   *     - `node_revision` ("old" revision -- our $this->revision).
   *
   * But the relationship structure is simpler:
   * - `$this->relationship` ("old" revision)
   *   - relationship ("new" revision).
   *
   * @return string
   */
  protected function get_table_alias_of_new_node_revision() {
    $preceding_revision_table_info =
      $this->query->get_table_info($this->relationship);

    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $new_revision_alias = $preceding_revision_table_info['relationship'];

    return $new_revision_alias;
  }

  /**
   * Gets information for querying all revision fields available on nodes.
   *
   * @return array
   *   An array containing query info arrays for each column (sub-property) of
   *   the field. Each key in the array uniquely identifies the field/property.
   *   Each value is a query info array containing the following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   */
  protected function get_all_node_fields_query_info() {
    module_load_include('inc', 'entity', 'includes/entity.property');
    module_load_include('inc', 'views', 'modules/field.views');

    $node_fields_query_info = array();
    $node_properties        = entity_get_all_property_info('node');

    foreach ($node_properties as $property_name => $node_property) {
      $current_query_info =
        $this->get_query_info_for_property($node_property, $property_name);

      $node_fields_query_info =
        array_merge($node_fields_query_info, $current_query_info);
    }

    return $node_fields_query_info;
  }

  /**
   * Gets query-info arrays describing the columns of a node property/field.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $property_name
   *   The machine name of the property.
   *
   * @return array
   *   An array containing query info arrays for each column (sub-property) of
   *   the field. Each key in the array uniquely identifies the field/property.
   *   Each value is a query info array containing the following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   */
  protected function get_query_info_for_property(array $property_info,
                                                 $property_name) {
    $result       = array();
    $is_computed  = $property_info['computed'] ?? FALSE;

    if (!$is_computed) {
      $schema_field = $property_info['schema field']  ?? NULL;
      $is_field     = $property_info['field']         ?? FALSE;

      if ($is_field) {
        // For clarity -- the property name is the field name in this context.
        $result =
          $this->get_query_info_for_field($property_info, $property_name);
      }
      elseif (($schema_field !== NULL) &&
              $this->is_revisioned_schema_field($schema_field)) {
        $result =
          $this->get_query_info_for_base_property(
            $property_info,
            $property_name,
            $schema_field
          );
      }
    }

    return $result;
  }

  /**
   * Convert information about a custom field into a query-info array.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return array
   *   An array containing query info arrays for each column (sub-property) of
   *   the field. Each key in the array is a colon-delimited tuple of the
   *   field name and column. Each value is a query info array containing the
   *   following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   */
  protected function get_query_info_for_field(array $property_info,
                                              $field_name) {
    $field_info = field_info_field($field_name);

    $storage_info = $field_info['storage']  ?? NULL;
    $storage_type = $storage_info['type']   ?? NULL;

    $revision_tables =
      $storage_info['details']['sql'][FIELD_LOAD_REVISION] ?? NULL;

    $result = array();

    // We only support SQL backends and revisioned fields
    if (($storage_type == 'field_sql_storage') && ($revision_tables !== NULL)) {
      $query_info =
        $this->get_query_info_for_sql_field(
          $revision_tables,
          $property_info,
          $field_name
        );

      $result = array_merge($result, $query_info);
    }

    return $result;
  }

  /**
   * Convert information about a custom SQL-based field into a query-info array.
   *
   * @param array $revision_tables
   *   Information about the table(s) to query for revisions of the field.
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return array
   *   An array containing query info arrays for each column (sub-property) of
   *   the field. Each key in the array is a colon-delimited tuple of the
   *   field name and column. Each value is a query info array containing the
   *   following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.

   *
   * @return array
   */
  protected function get_query_info_for_sql_field(array $revision_tables,
                                                  array $property_info,
                                                  string $field_name){
    $query_info     = array();
    $table_columns  = array();

    $field_sub_property_count = 0;

    foreach ($revision_tables as $table_name => $table_properties) {
      foreach ($table_properties as $sub_property_name => $table_column) {
        $column_info =
          $this->get_column_info_for_field(
            $property_info,
            $sub_property_name
          );

        if ($column_info !== NULL) {
          ++$field_sub_property_count;

          $table_columns[$table_name][] = array(
            'sub_property'  => $sub_property_name,
            'column'        => $table_column,
            'column_label'  => $column_info['label'],
          );
        }
      }
    }

    foreach ($table_columns as $table_name => $columns) {
      foreach ($columns as $column) {
        $sub_property_name  = $column['sub_property'];
        $table_column       = $column['column'];
        $column_label       = $column['column_label'];

        $column_query_info =
          $this->get_query_info_for_field_column(
            $property_info,
            $field_name,
            $field_sub_property_count,
            $sub_property_name,
            $table_name,
            $table_column,
            $column_label
          );

        $query_info = array_merge($query_info, $column_query_info);
      }
    }

    return $query_info;
  }

  /**
   * Convert information about a base field into a query-info array.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $property_name
   *   The machine name of the property.
   * @param string $field_name
   *   The machine name of the schema field that corresponds to the property.
   *
   * @return array
   *   A singleton array containing a query info array. The singleton key in the
   *   array is the field name. The singleton value is a query info array
   *   containing the following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   */
  protected function get_query_info_for_base_property(array $property_info,
                                                      $property_name,
                                                      $field_name) {
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $result = array(
      $property_name => array(
        'field_title'         => $property_info['label'],
        'field_name'          => $field_name,
        'table'               => 'node_revision',
        'entity_id_column'    => 'nid',
        'revision_id_column'  => 'vid',
        'value_column'        => $field_name,
      ),
    );

    return $result;
  }

  /**
   * Convert information about a custom field into a query-info array.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $field_name
   *   The machine name of the custom field.
   * @param int $field_sub_property_count
   *   The total number of properties this field has.
   * @param string $sub_property_name
   *   The name of the sub-property of the field. This is usually named very
   *   similarly to the table column, but without the field name prefix
   *   (e.g. "value").
   * @param string $table_name
   *   The name of the revision table for the custom field.
   * @param string $table_column
   *   The name of the column in the revision table for the field
   *   (e.g. "field_myfield_value").
   * @param string $column_label
   *   The human-friendly name of the column in the database.
   *
   * @return array
   *   A singleton array containing a query info array. The singleton key in the
   *   array is a colon-delimited tuple of the field name and sub-property. The
   *   singleton value is a query info array containing the following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   */
  protected function get_query_info_for_field_column(array $property_info,
                                                     $field_name,
                                                     $field_sub_property_count,
                                                     $sub_property_name,
                                                     $table_name,
                                                     $table_column,
                                                     $column_label) {
    $result = array();

    $key         = "{$field_name}:{$sub_property_name}";
    $field_label = $this->get_custom_field_label($property_info, $field_name);

    if ($field_sub_property_count > 1) {
      $title =
        t('@property (@column)',
          array(
            '@property' => $field_label,
            '@column'   => $column_label,
          )
        );
    }
    else {
      $title = $field_label;
    }

    $result[$key] = array(
      'field_title'         => $title,
      'field_name'          => $field_name,
      'table'               => $table_name,
      'entity_id_column'    => 'entity_id',
      'revision_id_column'  => 'revision_id',
      'value_column'        => $table_column,
    );

    return $result;
  }

  /**
   * Determine whether or not the specified base field is captured in revisions.
   *
   * This is used to skip immutable fields that only exist in the node, like
   * `type`, `language`, `created`, and `uuid`.
   *
   * @param string $field_name
   *   The machine name of the field to check.
   *
   * @return bool
   *   TRUE if the field appears in the `node_revisions` table; FALSE,
   *   otherwise.
   */
  protected function is_revisioned_schema_field($field_name) {
    $revision_schema = drupal_get_schema('node_revision');
    $revision_fields = $revision_schema['fields'];

    return (isset($revision_fields[$field_name]));
  }

  /**
   * Determines the appropriate label to use for a custom field.
   *
   * This handles special cases like the "body" field, which has an undesireable
   * property label. For these cases, we fall-back to the Field API of the
   * Views module to get a better label. That API comes with a performance
   * penalty, which is why we don't use it for all labels.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $field_name
   *   The machine name of the custom field.
   *
   * @return string
   *   The human-friendly label to use for the field.
   */
  protected function get_custom_field_label(array $property_info, $field_name) {
    $property_field_label = $property_info['label'];

    if ($property_field_label == t('The main body text')) {
      /** @noinspection PhpUnusedLocalVariableInspection */
      list($field_label, $_) = field_views_field_label($field_name);
    } else {
      $field_label = $property_field_label;
    }

    return $field_label;
  }

  /**
   * Builds a SQL query for getting revisions of a field based on query info.
   *
   * @param array $query_info
   *   A query info array containing the following keys:
   *     - field_title:         The human-friendly title for the property.
   *     - field_name:          The machine name of the field in the node
   *                            entity.
   *     - table:               The table to query for revisions to the field.
   *     - entity_id_column:    The name of the table column that references the
   *                            node ID.
   *     - revision_id_column:  The name of the table column that references the
   *                            revision ID of the node.
   *     - value_column:        The column in the revisions table containing the
   *                            field values.
   *
   * @return \SelectQuery|\SelectQueryInterface
   *   A query that will retrieve pairs of old and new versions for the field.
   */
  protected function build_field_query(array $query_info) {
    $field_title  = $query_info['field_title'];
    $field_name   = $query_info['field_name'];
    $table        = $query_info['table'];
    $nid_column   = $query_info['entity_id_column'];
    $vid_column   = $query_info['revision_id_column'];
    $value_column = $query_info['value_column'];

    $revision_map_alias = 'revision_sequence';
    $field_query = db_select('node_revision_sequence', $revision_map_alias);

    $placeholder_index = $this->field_placeholder_counter++;

    $new_alias =
      $field_query->addJoin(
        'INNER',
        $table,
        'new',
        "%alias.{$nid_column} = {$revision_map_alias}.nid ".
        "AND %alias.{$vid_column} = {$revision_map_alias}.new_vid"
      );

    $old_alias =
      $field_query->addJoin(
        'LEFT OUTER',
        $table,
        'old',
        "%alias.{$nid_column} = {$revision_map_alias}.nid ".
        "AND %alias.{$vid_column} = {$revision_map_alias}.old_vid"
      );

    $field_name_placeholder   = ':field_name_'  . $placeholder_index;
    $field_title_placeholder  = ':field_title_' . $placeholder_index;

    $field_query->addExpression(
      $field_name_placeholder,
      'field_name',
      array($field_name_placeholder => $field_name)
    );

    $field_query->addExpression(
      $field_title_placeholder,
      'field_title',
      array($field_title_placeholder => $field_title)
    );

    $field_query->addField($new_alias, $nid_column, 'entity_id');

    $field_query->addField($old_alias, $vid_column,  'old_vid');
    $field_query->addField($new_alias, $vid_column,  'new_vid');

    $field_query->addField($old_alias, $value_column,  'old_value');
    $field_query->addField($new_alias, $value_column,  'new_value');

    if ($table !== 'node_revision') {
      // Only return field values for nodes
      $field_query->where(
        "({$old_alias}.entity_type = 'node'".
        " OR {$old_alias}.entity_type IS NULL) ".
        "AND {$new_alias}.entity_type = 'node'"
      );
    }

    $field_query->where(
      "({$old_alias}.{$value_column} IS NULL".
      " AND {$new_alias}.{$value_column} IS NOT NULL) ".
      "OR ({$old_alias}.{$value_column} <> {$new_alias}.{$value_column})");

    return $field_query;
  }

  /**
   * Gets column information for a specific sub-property of a field.
   *
   * This is needed to work around inconsistencies in how certain types of
   * fields (e.g. "text" fields) are handled by Entity API. Normally, these
   * fields lack a "property info" key for reasons unbeknownst to me; that key
   * is needed for this handler to build a query for the values of
   * sub-properties/columns.
   *
   * @param array $property_info
   *   The array of information about the specific property, in the structure
   *   returned by entity_get_all_property_info().
   * @param string $sub_property_name
   *   The name of the sub-property of the field. This is usually named very
   *   similarly to the table column, but without the field name prefix
   *   (e.g. "value").

   * @return array
   *   The column information about the sub-property of the property.
   */
  protected function get_column_info_for_field(array $property_info,
                                               $sub_property_name) {
    $columns = $property_info['property info'] ?? NULL;

    // BUGBUG: Why do text fields have no 'property info'?
    if (($columns === NULL) && ($property_info['type'] === 'text')) {
      $default_property_info = entity_property_text_formatted_info();
      $columns = array('value' => $default_property_info['value']);
    }

    return $columns[$sub_property_name] ?? NULL;
  }

  /**
   * Filters-out node reference relationships that are not preceding revisions.
   *
   * The "Field differences" relationship requires the use of the
   * node_revision_sequence table (exposed by the "Preceding revision"
   * relationship) for performance reasons. Otherwise, the query would not be
   * tractable due to the database having to compute the cross-product of all
   * possible old and new revisions. For example, if node 1 has revisions A, B,
   * C, and D, this ensures we only calculate the differences between the
   * following (4 results -- O(N) where N is the number of revisions):
   *  - Nothing and A
   *  - A and B
   *  - B and C
   *  - C and D
   *
   * ...rather than (10 results -- O(N^2) where N is the number of revisions):
   *  - Nothing and A
   *  - Nothing and B
   *  - Nothing and C
   *  - Nothing and D
   *  - A and B
   *  - A and C
   *  - A and D
   *  - B and C
   *  - B and D
   *  - C and D
   *
   * The more revisions in the database for a given node, the larger the
   * explosion of diffs in the second case.
   *
   * @param array $form
   *   A reference to the relationship configuration form.
   */
  protected function filter_relationship_options(array &$form) {
    $all_relationships =
      $this->view->display_handler->get_option('relationships');

    $relationship_selector  =& $form['relationship'];
    $relationship_options   =& $relationship_selector['#options'];

    $relationship_selector['#title'] = t('Relationship (older revision)');

    $relationship_selector['#description'] =
      t('Select the relationship that provides information about the "older" revision. This is the revision against which data in the "newer" revision will be compared. For performance reasons, field differences can only be calculated between two revisions in sequence.');

    // Filter-out all relationship options that are not linked to the
    // "node_revision_sequence" table.
    $relationship_options =
      array_filter(
        $relationship_options,
        function ($key) use ($all_relationships) {
          $target_table = $all_relationships[$key]['table'] ?? NULL;

          return ($target_table === 'node_revision_sequence');
        },
        ARRAY_FILTER_USE_KEY
      );
  }

  /**
   * Builds the selector used in the Views Admin UI to pick fields for diff.
   *
   * Selecting exactly which fields to include cuts down on unwanted fields in
   * the diff output and has a direct impact on query performance by removing
   * JOINs to fields from the diff query.
   *
   * @return array
   *   The form element for selecting the fields desired for diff output.
   */
  protected function build_diff_fields_selector() {
    $node_field_info  = $this->get_all_node_fields_query_info();
    $field_options    = array();

    foreach ($node_field_info as $field_id => $current_field_info) {
      $field_options[$field_id] =
        t('@title (@field_name)',
          array(
            '@title'      => $current_field_info['field_title'],
            '@field_name' => $current_field_info['field_name'],
          )
        );
    }

    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $field_selector = array(
      '#type'           => 'checkboxes',
      '#title'          => t('Field differences to include'),
      '#description'    => t('For maximum performance, select which fields should be included when calculating field differences. If nothing is selected, all node fields will be included.'),
      '#options'        => $field_options,
      '#checkall'       => TRUE,
      '#default_value'  => $this->options['diff_fields'],
    );

    return $field_selector;
  }
}
