<?php
/**
 * @file
 *   Admin UI for the "Revision Field Views" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */

//==============================================================================
// Form Builders
//==============================================================================

/**
 * Form builder for the configuration form of this module.
 *
 * @param array $form
 *   The settings form array to populate.
 * @param array $form_state
 *   A reference to the form state for the settings form.
 *
 * @return array
 *   The built form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function revision_field_views_config_form($form, &$form_state) {
  $form['help'] = array(
    '#type'   => 'markup',
    '#prefix' => '<p>',
    '#markup' => t('The node revision sequence is rebuilt automatically on cron runs. However, you can rebuild it immediately from this page.'),
    '#suffix' => '</p>',
  );

  $form['rebuild_sequence'] = array(
    '#type'   => 'submit',
    '#value'  => t('Rebuild node revision sequence'),
    '#submit' => array('revision_field_views_rebuild_submit'),
  );

  return $form;
}

/**
 * Submit callback for the "Rebuild node revision sequence" button.
 */
function revision_field_views_rebuild_submit() {
  revision_field_views_rebuild_revision_sequence();
  drupal_set_message(t('Node revision sequence has been rebuilt.'));
}
