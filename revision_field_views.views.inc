<?php
/**
 * @file
 *   Views interface definitions for the "Revision Field Views" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @ingroup views_field_handlers
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */

/**
 * Implements hook_views_data().
 */
function revision_field_views_views_data() {
  $data = array();

  $data['node_revision_sequence']['table'] = array(
    'group' => t('Content revision'),
    'join'  => array(
      'node_revision' => array(
        // From node_revision_sequence.new_vid
        'field' => 'new_vid',

        // To node_revision.vid
        'left_table' => 'node_revision',
        'left_field' => 'vid'
      ),
    ),
  );

  $data['node_revision_sequence']['old_vid'] = array(
    'title' => t('Preceding revision ID'),
    'relationship' => array(
      'title'   => t('Preceding revision'),
      'label'   => t('Preceding revision'),
      'help'    => t('Relate the content revision to the revision that preceded it.'),
      'handler' => 'views_handler_relationship',

      // From node_revision_sequence.old_vid to node_revision.vid
      'base'        => 'node_revision',
      'base field'  => 'vid',
    ),
  );

  $data['revision_diff']['table'] = array(
    'group' => t('Content revision'),
    'join'  => array(
      'node_revision' => array(
        'left_table' => 'node_revision',
      ),
    ),
  );

  $data['revision_diff']['diff'] = array(
    'title' => t('Field differences'),
    'relationship' => array(
      'label'   => t('Revision diff'),
      'help'    => t('Relate a revision to the field values that changed since that revision.'),
      'base'    => 'revision_diff',
      'handler' => '\Drupal\revision_field_views\Views\views_handler_relationship_diff',
    ),
  );

  $data['revision_diff_field']['table'] = array(
    'group' => t('Content revision diff'),
    'join'  => array(
      'revision_diff' => array(
        'left_table' => 'revision_diff',
      )
    ),
  );

  $data['revision_diff_field']['field_name'] = array(
    'title'     => t('Field machine name'),
    'help'      => t('The machine name of the field modified in the revision.'),
    'field'     => array(
      'handler'         => 'views_handler_field',
      'click sortable'  => TRUE,
    ),
    'sort'      => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'    => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument'  => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['revision_diff_field']['field_title'] = array(
    'title'     => t('Field title'),
    'help'      => t('The human-friendly title of the field modified in the revision.'),
    'field'     => array(
      'handler'         => 'views_handler_field',
      'click sortable'  => TRUE,
    ),
    'sort'      => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'    => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument'  => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['revision_diff_field']['old_value'] = array(
    'title'     => t('Old field value'),
    'help'      => t('The value of the field prior to the revision.'),
    'field'     => array(
      'handler'         => 'views_handler_field',
      'click sortable'  => TRUE,
    ),
    'sort'      => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'    => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument'  => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['revision_diff_field']['new_value'] = array(
    'title'     => t('New field value'),
    'help'      => t('The value of the field set in the revision.'),
    'field'     => array(
      'handler'         => 'views_handler_field',
      'click sortable'  => TRUE,
    ),
    'sort'      => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'    => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument'  => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}
