<?php
/**
 * @file
 *   Constants for the "Revision Field Views" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */

/**
 * The machine name for this module.
 *
 * @var string
 */
const REFIVI_MODULE_NAME = 'revision_field_views';
