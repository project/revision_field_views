<?php
/**
 * @file
 *   Main module code for the "Revision Field Views" module.
 *
 *   © 2020 Inveniem. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 */

require_once('revision_field_views.constants.inc');

//==============================================================================
// Hook Implementations
//==============================================================================

/**
 * Implements hook_views_api().
 */
function revision_field_views_views_api() {
  return array(
    'api'   => 3,
    'path'  => drupal_get_path('module', REFIVI_MODULE_NAME),
  );
}

/**
 * Implements hook_cron().
 */
function revision_field_views_cron() {
  revision_field_views_rebuild_revision_sequence();
}

/**
 * Implements hook_menu().
 */
function revision_field_views_menu() {
  $items['admin/config/content/revision_field_views'] = array(
    'title'             => 'Revision field views',
    'description'       => 'Manage the data used to show the differences between node revisions.',
    'type'              => MENU_NORMAL_ITEM,
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('revision_field_views_config_form'),
    'access arguments'  => array('administer site configuration'),
    'file'              => 'revision_field_views.admin.inc',
  );

  return $items;
}

//==============================================================================
// Public API
//==============================================================================

/**
 * Truncates and re-populates the node revision sequence table.
 *
 * The order of all revisions in the system is queried and used to populate the
 * `node_revision_sequence` table.
 */
function revision_field_views_rebuild_revision_sequence() {
  $transaction = db_transaction();

  try {
    db_truncate('node_revision_sequence')
      ->execute();

    $revision_query = db_select('node_revision', 'new');

    $revision_query->addJoin(
      'LEFT OUTER',
      'node_revision',
      'old',
      '(new.nid = old.nid) AND (old.vid < new.vid)'
    );

    $previous_rev_subquery = db_select('node_revision', 'previous_revision_sq');

    $previous_rev_subquery->addField('previous_revision_sq', 'vid');

    $previous_rev_subquery
      ->where("previous_revision_sq.nid = new.nid")
      ->where("previous_revision_sq.vid < new.vid")
      ->orderBy('previous_revision_sq.vid', 'DESC')
      ->range(0, 1);

    $revision_query->where(
      "old.vid IS NULL OR old.vid = ({$previous_rev_subquery})"
    );

    $revision_query->addField('new', 'nid');
    $revision_query->addField('old', 'vid', 'old_vid');
    $revision_query->addField('new', 'vid', 'new_vid');

    db_insert('node_revision_sequence')
      ->from($revision_query)
      ->execute();
  }
  catch (\Exception $ex) {
    $transaction->rollback();

    throw $ex;
  }
}
